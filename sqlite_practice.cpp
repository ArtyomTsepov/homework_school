#include <stdio.h>
#include "sqlite3.h"
#include <iostream>
using namespace std;
const char* SQL = "CREATE TABLE Department (id int auto_increment primary key, name varchar(100)); \
CREATE TABLE Employee (id int auto_increment primary key,department_id int,chief_id int,name varchar(100),salary int,FOREIGN KEY(department_id) REFERENCES Department(id),FOREIGN KEY(chief_id) REFERENCES Employee(id));\
INSERT INTO Department VALUES (1, 'robototechniki'); INSERT INTO Department VALUES(2, 'shaurmichny');\
INSERT INTO Department VALUES(3, 'kotikov'); \
INSERT INTO Department VALUES(4, 'schastia'); \
INSERT INTO Employee(id, department_id, chief_id, name, salary) VALUES(1, 1, 1, 'Petya', 100);\
INSERT INTO Employee(id, department_id, chief_id, name, salary) VALUES(2, 2, 1, 'Valya', 100);\
INSERT INTO Employee(id, department_id, chief_id, name, salary) VALUES(3, 4, 1, 'Anya', 400);\
INSERT INTO Employee(id, department_id, chief_id, name, salary) VALUES(4, 3, 1, 'Sonya', 500);\
INSERT INTO Employee(id, department_id, chief_id, name, salary) VALUES(5, 2, 2, 'Rita', 500);\
INSERT INTO Employee(id, department_id, chief_id, name, salary) VALUES(6, 2, 2, 'Kesha', 200);\
INSERT INTO Employee(id, department_id, chief_id, name, salary) VALUES(7, 3, 4, 'Gena', 300);\
INSERT INTO Employee(id, department_id, chief_id, name, salary) VALUES(8, 2, 4, 'Varya', 900); ";
const char *sql1 = "select * from Department; select * from Employee";
const char* sq1 = "select name from Employee  where salary >(select  salary from Employee as emp where emp.id = employee.chief_id and employee.salary > emp.salary)";  //first
const char* sq2 = "select name from employee where salary = (select max(salary) from employee as emp where emp.department_id = employee.department_id)";               //second
const char* sq3 = "select department_id from Employee group by department_id having count(department_id)<=3";                                                          //third
const char* sq4 = "select name from employee where department_id <>(select department_id from employee as emp where emp.id = employee.chief_id)";                      //fourth
const char* sq5 = "select department_id from (select sum(salary) as sum_salary, department_id from employee group by department_id) where sum_salary in (select max(sum_salary) from (select sum(salary) as sum_salary, department_id from employee group by department_id))";
static int callback(void *data, int argc, char **argv, char **azColName) {
	int i;
	for (i = 0; i<argc; i++) {
		printf("%s ", argv[i] ? argv[i] : "NULL");
	}
	printf("\n");
	return 0;
}


int main(int argc, char **argv) {

	sqlite3 *db = 0; // ����� ������� ���������� � ��
	char *err = 0;
	// ��������� ����������
	const char* data = "Callback function called";
	int n;
	cin >> n;
	if (sqlite3_open("my_cosy_database.dblite", &db))
		fprintf(stderr, "������ ��������/�������� ��: %s\n", sqlite3_errmsg(db));
	// ��������� SQL
	else 
	{
		if (n == 0 && sqlite3_exec(db, SQL, 0, 0, &err))
		{
			fprintf(stderr, "������ SQL: %sn", err);
			sqlite3_free(err);
		}
		if (n == 1 && sqlite3_exec(db, sq5, callback, 0, &err))
		{
			fprintf(stderr, "������ SQL: %sn", err);
			sqlite3_free(err);
		}
	}
	// ��������� ����������
	sqlite3_close(db);
	return 0;
}