#include <stdio.h>
#include "sqlite3.h"
#include <iostream>
using namespace std;
const char* SQL = "CREATE TABLE workers(id, login, salary, age, date, description); \
insert into workers(id, login, salary, age, date, description) values (1, 'Dima', 100, 22, '2016-12-12 00:12:12', 'a'); \
insert into workers(id, login, salary, age, date, description) values (2, 'Petya', 200, 23, '2016-12-13 00:13:13', 'b'); \
insert into workers(id, login, salary, age, date, description) values (3, 'Vasya', 300, 24, '2016-12-14 00:14:14', 'c'); \
insert into workers(id, login, salary, age, date, description) values (4, 'Kolya', 400, 25, '2017-12-15 00:15:15', 'd'); \
insert into workers(id, login, salary, age, date, description) values (5, 'Ivan', 500, 26, '2017-12-16 00:16:16', 'e'); \
insert into workers(id, login, salary, age, date, description) values (6, 'Kirill', 600, 27, '2017-12-17 00:17:17', 'f');";


const char* sql16 = "Select * from workers where date > date('now')";
const char* sql20 = "Select * from workers where strftime('%Y', date) = '2016'";
const char* sql21 = "Select * from workers where strftime('%m', date) = '03'";
const char* sql22 = "Select * from workers where strftime('%d', date) = '03'";
const char* sql23 = "Select * from workers where strftime('%d', date) = '05' and strftime('m', date) = '04'";
const char* sql24 = "Select * from workers where strftime('%d', date) in ('01', '07', '11', '12', '15', '19', '21', '29')";
const char* sql25 = "Select * from workers where strftime('%w', date) = '2'";
const char* sql26 = "Select * from workers where strftime('%d', date) >= '01' and strftime('%d', date) <= '10'";
const char* sql27 = "Select * from workers where strftime('%d', date) < strftime('%m', date)";
const char* sql28 = "Select *, strftime('%d', date), strftime('%m', date), strftime('%Y', date)  from workers";
const char* sql29 = "Select *, strftime('%w', date) as today  from workers";

static int callback(void *data, int argc, char **argv, char **azColName) {
	int i;
	for (i = 0; i<argc; i++) {
		printf("%s ", argv[i] ? argv[i] : "NULL");
	}
	printf("\n");
	return 0;
}


int main(int argc, char **argv) {

	sqlite3 *db = 0; // ����� ������� ���������� � ��
	char *err = 0;
	// ��������� ����������
	const char* data = "Callback function called";
	int n;
	cin >> n;
	if (sqlite3_open("my_cosy_database.dblite", &db))
		fprintf(stderr, "������ ��������/�������� ��: %s\n", sqlite3_errmsg(db));
	// ��������� SQL
	else 
	{
		if (n == 0 && sqlite3_exec(db, SQL, 0, 0, &err))
		{
			fprintf(stderr, "������ SQL: %sn", err);
			sqlite3_free(err);
		}
		if (n == 1 && sqlite3_exec(db, sql30, callback, 0, &err))
		{
			fprintf(stderr, "������ SQL: %sn", err);
			sqlite3_free(err);
		}
	}
	// ��������� ����������
	sqlite3_close(db);
	return 0;
}