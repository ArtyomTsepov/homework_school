#include <iostream>
#include <algorithm>
#include <vector>


using namespace std;

int main()
{
	vector<pair<int, int>> a(5);
	for (int i = 0; i < 5;i++)
	{
		cin >> a[i].first;
		a[i].second = i;
	}
	int ans1 = 0, ans2 = 0;
	sort(a.begin(), a.end());
	ans1 = a[0].first * 4 + a[1].first * 4 + a[2].first * 3 + a[3].first * 2 + a[4].first;
	ans2 = a[0].first * 3 + a[1].first * 3 + a[2].first * 2 + a[3].first * 2 + a[4].first*2;
	vector<vector<int>> answ(5);
	if (ans1 < ans2)
	{
			answ[a[0].second] = { 0, 0, 0, 0 };
			answ[a[1].second] = { 0, 0, 0, 1 };
			answ[a[2].second] = { 0, 0, 1 };
			answ[a[3].second] = { 0, 1 };
			answ[a[4].second] = { 1 };
	}
	else
	{
		answ[a[0].second] = { 1, 0, 1 };
		answ[a[1].second] = { 1, 0, 0 };
		answ[a[2].second] = { 1, 1 };
		answ[a[3].second] = { 0, 1 };
		answ[a[4].second] = { 0, 0 };
	}
	for (int i = 0; i < 5; i++)
	{
		for (int j = 0; j < answ[i].size(); j++)
		{
			cout << answ[i][j];
		}
		cout << endl;
	}
	return 0;
}