#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
#include <string>
#include <ctime>
#include <cstdlib>
#include <thread>
#include <chrono>
using namespace std;
int size_n = 100000, max_n = 1000000000, size_m = 10000;    //enter your values N and M


vector<vector<int>> c(4);
vector<int> a;

void s(int k)
{
	sort(c[k].begin(), c[k].end());
}


int main()
{
	//srand(time(0));
	ofstream fout("input.txt");
	for (int i = 0; i < size_n; i++)
	{
		fout << (rand()*rand()) % max_n << "\n";						//generate big file with numbers 1..10^9
	}
	vector<ofstream> g;
	string hh;
	int size = size_n / size_m;
	if (size_n%size_m != 0)												//find the number of files
	{
		size++;
	}
	for (int i = 0; i < size; i++)										//generate files
	{
		hh = to_string(i);
		hh += ".txt";
		g.push_back(ofstream(hh));
	}
	vector<int> ss(size);
	for (int i = 0; i < size; i++) ss[i] = size_m;						//find size of each file
	ss[size - 1] = size_n%size_m;
	if (ss[size - 1] == 0)
	{
		ss[size - 1] = size_m;
	}
	vector<int> a;
	fout.close();
	ifstream fin("input.txt");
	for (int i = 0; i < size; i++)										//cin in small files from big
	{
		if (i == size - 1)
		{
			hh = to_string(i);
			hh += ".txt";
			fout.open(hh);
			int x;
			for (int j = 0; j < ss[i]; j++)
			{
				fin >> x;
				a.push_back(x);
			}
			sort(a.begin(), a.end());
			for (int j = 0; j < a.size(); j++)
			{
				fout << a[j] << "\n";
			}
			a.clear();
			fout.close();
		}
		else
		{
			hh = to_string(i);
			hh += ".txt";
			fout.open(hh);
			int x;
			for (int j = 0; j < ss[i]; j++)
			{
				fin >> x;
				a.push_back(x);
			}
			/////////////////////////										We used multithreading in sorting our small files
			for (int i = 0; i < 4; i++)
			{
				c[i].clear();
				for (int j = 0; j < a.size() / 4; j++)
				{
					c[i].push_back(a[i*(a.size() / 4) + j]);
				}
			}
			thread x1(s, 0);
			thread x2(s, 1);
			thread x3(s, 2);
			thread x4(s, 3);
			x1.join();
			x2.join();
			x3.join();
			x4.join();
			sort(a.begin(), a.end());
			vector<int> siz(4, a.size() / 4), used(4, 0);
			int ll = 0;
			while (ll != a.size())
			{
				int minn = 1000000007, mini = -1;
				for (int i = 0; i < 4; i++)
				{
					if (used[i] < siz[i])
					{
						if (c[i][used[i]] < minn)
						{
							minn = c[i][used[i]];
							mini = i;
						}
					}
				}
				fout << minn << "\n";
				used[mini]++;
				ll++;
			}
			/////////////////////////												cout our already sorting file
			for (int j = 0; j < a.size(); j++)
			{
				fout << a[j] << "\n";
			}
			a.clear();
			fout.close();
		}
	}

	vector<ifstream> file(size);
	for (int i = 0; i < size; i++)
	{
		hh = to_string(i);
		hh += ".txt";
		file[i].open(hh);
	}

	int s = 0;
	vector<int> last(size), used(size, 0), sizee(size);
	for (int i = 0; i < size; i++)										//sort each small file
	{
		file[i] >> last[i];
		sizee[i] = size_m;
		if (i == size - 1)
		{
			sizee[i] = size_n%size_m;
			if (sizee[i] == 0)
			{
				sizee[i] = size_m;
			}
		}
	}
	fout.open("output.txt");
	while (s != size_n)										//while not output all elements
	{
		int mini = 1000000007, mins = 0;
		for (int j = 0; j < size; j++)						//find smallest
		{
			if (used[j] < sizee[j])
			{
				if (mini > last[j])
				{
					mini = last[j];
					mins = j;
				}
			}
		}
		fout << mini << "\n";								//merge
		used[mins]++;
		file[mins] >> last[mins];
		s++;
	}

	return 0;
}