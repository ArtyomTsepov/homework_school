#include <iostream>
#include <algorithm>
#include <vector>


using namespace std;

int main()
{
	vector<int> a(15);
	for (int i = 0; i < 15;i++)
	{
		cin >> a[i];
	}
	vector<int> b(20), used(20);
	used[0] = 1;
	used[1] = 1;
	used[3] = 1;
	used[7] = 1;
	used[15] = 1;
	int k = 0;
	for (int i = 0; i < 20;i++)
	{
		if (!used[i])
		{
			b[i] = a[k];
			k++;
		}
		else
		{
			b[i] = 0;
		}
	}
	used[0] = b[0] + b[2] + b[4] + b[6] + b[8] + b[10] + b[12] + b[14] + b[16] + b[18];
	used[1] = b[1] + b[2] + b[4] + b[5] + b[7] + b[8] + b[10] + b[11] + b[13] + b[14] + b[16] + b[17] + b[19];
	used[3] = b[3] + b[4] + b[5] + b[6] + b[11] + b[12] + b[13] + b[14] + b[19];
	used[7] = b[7] + b[8] + b[9] + b[10] + b[11] + b[12] + b[13] + b[14];
	used[15] = b[15] + b[16] + b[17] + b[18] + b[19];
	b[0] = used[0] % 2;
	b[1] = used[1] % 2;
	b[3] = used[3] % 2;
	b[7] = used[7] % 2;
	b[15] = used[15] % 2;
	for (int i = 0; i < 20; i++)
	{
		cout << b[i] << " ";
	}
	return 0;
}