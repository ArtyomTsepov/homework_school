#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
#include <string>
#include <ctime>
using namespace std;
int size_n = 10000, max_n = 1000000000, size_m = 1000; //enter your values N and M


int main()
{
	//srand(time(0));
	ofstream fout("input.txt");
	for (int i = 0; i < size_n; i++)
	{
		fout << (rand()*rand()) % max_n << "\n"; //generate big file with numbers 1..10^9
	}
	vector<ofstream> g;
	string hh;
	int size = size_n / size_m;
	if (size_n%size_m != 0) //find the number of files
	{
		size++;
	}
	for (int i = 0; i < size; i++) //generate files
	{
		hh = to_string(i);
		hh += ".txt";
		g.push_back(ofstream(hh));
	}
	vector<int> ss(size);
	for (int i = 0; i < size; i++) ss[i] = size_m; //find size of each file
	ss[size - 1] = size_n%size_m;
	if (ss[size - 1] == 0)
	{
		ss[size - 1] = size_m;
	}
	vector<int> a;
	fout.close();
	ifstream fin("input.txt");
	for (int i = 0; i < size; i++) //cin in small files from big
	{
		hh = to_string(i);
		hh += ".txt";
		fout.open(hh);
		int x;
		for (int j = 0; j < ss[i]; j++)
		{
			fin >> x;
			a.push_back(x);
		}
		sort(a.begin(), a.end());
		for (int j = 0; j < a.size(); j++)
		{
			fout << a[j] << "\n";
		}
		a.clear();
		fout.close();
	}

	vector<ifstream> file(size);
	for (int i = 0; i < size; i++)
	{
		hh = to_string(i);
		hh += ".txt";
		file[i].open(hh);
	}

	int s = 0;
	vector<int> last(size), used(size, 0), sizee(size);
	for (int i = 0; i < size; i++) //sort each small file
	{
		file[i] >> last[i];
		sizee[i] = size_m;
		if (i == size - 1)
		{
			sizee[i] = size_n%size_m;
			if (sizee[i] == 0)
			{
				sizee[i] = size_m;
			}
		}
	}
	fout.open("output.txt");
	while (s != size_n) //while not output all elements
	{
		int mini = 1000000007, mins = 0;
		for (int j = 0; j < size; j++) //find smallest
		{
			if (used[j] < sizee[j])
			{
				if (mini > last[j])
				{
					mini = last[j];
					mins = j;
				}
			}
		}
		fout << mini << "\n"; //merge
		used[mins]++;
		file[mins] >> last[mins];
		s++;
	}

	return 0;
}